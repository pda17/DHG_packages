Source: haskell-wreq
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev,
 libghc-aeson-dev (>= 0.7.0.3),
 libghc-aeson-pretty-dev (>= 0.7.1),
 libghc-aeson-prof,
 libghc-attoparsec-dev (>= 0.11.1.0),
 libghc-attoparsec-prof,
 libghc-authenticate-oauth-dev (>= 1.5),
 libghc-authenticate-oauth-prof,
 libghc-base16-bytestring-dev,
 libghc-base16-bytestring-prof,
 libghc-case-insensitive-dev,
 libghc-case-insensitive-prof,
 libghc-cryptonite-dev,
 libghc-cryptonite-prof,
 libghc-exceptions-dev (>= 0.5),
 libghc-exceptions-prof,
 libghc-hashable-dev,
 libghc-hashable-prof,
 libghc-http-client-dev (>= 0.6),
 libghc-http-client-prof,
 libghc-http-client-tls-dev (>= 0.3.3),
 libghc-http-client-tls-prof,
 libghc-http-types-dev,
 libghc-http-types-dev (>= 0.8),
 libghc-http-types-prof,
 libghc-hunit-dev,
 libghc-lens-aeson-dev,
 libghc-lens-aeson-prof,
 libghc-memory-dev,
 libghc-memory-prof,
 libghc-mime-types-dev,
 libghc-mime-types-prof,
 libghc-network-info-dev,
 libghc-psqueues-dev (>= 0.2),
 libghc-psqueues-prof,
 libghc-quickcheck2-dev (>= 2.7),
 libghc-aeson-dev,
 libghc-aeson-pretty-dev (>= 0.8.0),
 libghc-base64-bytestring-dev,
 libghc-doctest-dev,
 libghc-http-client-dev,
 libghc-http-types-dev,
 libghc-lens-dev,
 libghc-network-info-dev,
 libghc-snap-core-dev (>= 1.0.0.0),
 libghc-snap-server-dev (>= 0.9.4.4),
 libghc-temporary-dev,
 libghc-test-framework-dev,
 libghc-test-framework-hunit-dev,
 libghc-test-framework-quickcheck2-dev,
 libghc-time-locale-compat-dev,
 libghc-time-locale-compat-prof,
 libghc-unix-compat-dev,
 libghc-unordered-containers-dev,
 libghc-unordered-containers-prof,
 libghc-uuid-dev,
 libghc-vector-dev,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-authenticate-oauth-doc,
 libghc-base16-bytestring-doc,
 libghc-case-insensitive-doc,
 libghc-cryptonite-doc,
 libghc-exceptions-doc,
 libghc-hashable-doc,
 libghc-http-client-doc,
 libghc-http-client-tls-doc,
 libghc-http-types-doc,
 libghc-lens-aeson-doc,
 libghc-memory-doc,
 libghc-mime-types-doc,
 libghc-psqueues-doc,
 libghc-time-locale-compat-doc,
 libghc-unordered-containers-doc,
Standards-Version: 4.5.0
Homepage: http://www.serpentine.com/wreq
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-wreq
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-wreq]
X-Description: easy-to-use HTTP client library
 A web client library that is designed for ease of use.
 .
 Features include:
  * Simple but powerful `lens`-based API
  * A solid test suite, and built on reliable libraries like
    http-client and lens
  * Session handling includes connection keep-alive and pooling, and
    cookie persistence
  * Automatic response body decompression
  * Powerful multipart form and file upload handling
  * Support for JSON requests and responses, including navigation of
    schema-less responses
  * Basic and OAuth2 bearer authentication
  * Early TLS support via the tls package

Package: libghc-wreq-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-wreq-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-wreq-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
