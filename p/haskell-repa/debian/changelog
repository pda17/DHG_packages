haskell-repa (3.4.1.4-3) unstable; urgency=medium

  * Sourceful upload for GHC 8.8
  * Bump dependency bounds

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 17 Jun 2020 11:14:33 +0300

haskell-repa (3.4.1.4-2) unstable; urgency=medium

  * Bump quickcheck bounds

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 26 Aug 2019 16:51:50 +0200

haskell-repa (3.4.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 28 Jul 2019 15:58:38 -0400

haskell-repa (3.4.1.3-3) unstable; urgency=medium

  * Newer build-deps from hackage

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Thu, 01 Nov 2018 13:42:37 +0200

haskell-repa (3.4.1.3-2) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:19:47 +0300

haskell-repa (3.4.1.3-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 16:57:01 -0400

haskell-repa (3.4.1.2-3) unstable; urgency=medium

  * Patch in order to work with newer versions of haskell-vector
  * Update d/copyright file
    - Set The Data Parallel Haskell Team as the copyright holder
    - Add Debian contributors under debian/* files

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Tue, 17 Oct 2017 16:08:53 +0300

haskell-repa (3.4.1.2-2) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:35:52 -0400

haskell-repa (3.4.1.2-1) experimental; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Wed, 26 Oct 2016 19:12:27 -0400

haskell-repa (3.4.1.1-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 16:23:26 -0400

haskell-repa (3.4.1.1-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 17:10:07 -0400

haskell-repa (3.4.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 10 Jan 2016 23:45:26 -0500

haskell-repa (3.4.0.1-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:55:00 -0500

haskell-repa (3.4.0.1-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:36 +0200

haskell-repa (3.3.1.2-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:53:04 +0200

haskell-repa (3.3.1.2-1) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:11:57 +0100

haskell-repa (3.2.3.3-3) unstable; urgency=low

  * Adjust watch file to new hackage layout
  * Allow QuickCheck 2.7

 -- Joachim Breitner <nomeata@debian.org>  Wed, 30 Jul 2014 22:56:18 +0200

haskell-repa (3.2.3.3-2) unstable; urgency=low

  * Build-depend on ghc-ghci, due to use of Template Haskell.

 -- Colin Watson <cjwatson@debian.org>  Sat, 15 Jun 2013 13:06:01 +0100

haskell-repa (3.2.3.3-1) unstable; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Fri, 31 May 2013 10:23:30 +0200

haskell-repa (3.2.3.1-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:51:52 +0200

haskell-repa (3.2.3.1-1) experimental; urgency=low

  [ Denis Laxalde ]
  * Initial release. (Closes: #701042)

 -- Joachim Breitner <nomeata@debian.org>  Thu, 28 Feb 2013 10:22:18 +0100
