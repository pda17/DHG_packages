Source: haskell-classy-prelude
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-async-dev,
 libghc-async-prof,
 libghc-basic-prelude-dev (>= 0.7),
 libghc-basic-prelude-prof,
 libghc-bifunctors-dev,
 libghc-bifunctors-prof,
 libghc-chunked-data-dev (>= 0.3),
 libghc-chunked-data-prof,
 libghc-dlist-dev (>= 0.7),
 libghc-dlist-prof,
 libghc-hashable-dev,
 libghc-hashable-prof,
 libghc-mono-traversable-dev (>= 1.0),
 libghc-mono-traversable-instances-dev,
 libghc-mono-traversable-instances-prof,
 libghc-mono-traversable-prof,
 libghc-mutable-containers-dev (<< 0.4),
 libghc-mutable-containers-dev (>= 0.3),
 libghc-mutable-containers-prof,
 libghc-primitive-dev,
 libghc-primitive-prof,
 libghc-say-dev,
 libghc-say-prof,
 libghc-semigroups-dev,
 libghc-semigroups-prof,
 libghc-stm-chans-dev (>= 3),
 libghc-stm-chans-prof,
 libghc-unliftio-dev (>= 0.2.1.0),
 libghc-unliftio-prof,
 libghc-unordered-containers-dev,
 libghc-unordered-containers-prof,
 libghc-vector-dev,
 libghc-vector-instances-dev,
 libghc-vector-instances-prof,
 libghc-vector-prof,
 libghc-hspec-dev (>= 1.3),
 libghc-quickcheck2-dev,
Build-Depends-Indep:
 ghc-doc,
 libghc-async-doc,
 libghc-basic-prelude-doc,
 libghc-bifunctors-doc,
 libghc-chunked-data-doc,
 libghc-dlist-doc,
 libghc-hashable-doc,
 libghc-mono-traversable-doc,
 libghc-mono-traversable-instances-doc,
 libghc-mutable-containers-doc,
 libghc-primitive-doc,
 libghc-say-doc,
 libghc-semigroups-doc,
 libghc-stm-chans-doc,
 libghc-unliftio-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
 libghc-vector-instances-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/snoyberg/mono-traversable#readme
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-classy-prelude
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-classy-prelude]

Package: libghc-classy-prelude-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: typeclass-based prelude${haskell:ShortBlurb}
 Focuses on using common typeclasses when possible, and creating new ones
 to avoid name clashing. Exposes many recommended datastructures (Map,
 ByteString, etc) directly without requiring long import lists and
 qualified modules.
 .
 ${haskell:Blurb}

Package: libghc-classy-prelude-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: typeclass-based prelude${haskell:ShortBlurb}
 Focuses on using common typeclasses when possible, and creating new ones
 to avoid name clashing. Exposes many recommended datastructures (Map,
 ByteString, etc) directly without requiring long import lists and
 qualified modules.
 .
 ${haskell:Blurb}

Package: libghc-classy-prelude-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: typeclass-based prelude${haskell:ShortBlurb}
 Focuses on using common typeclasses when possible, and creating new ones
 to avoid name clashing. Exposes many recommended datastructures (Map,
 ByteString, etc) directly without requiring long import lists and
 qualified modules.
 .
 ${haskell:Blurb}
