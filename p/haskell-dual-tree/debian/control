Source: haskell-dual-tree
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 haskell-devscripts (>= 0.13),
 ghc (>= 8),
 ghc-prof,
 libghc-monoid-extras-dev (>= 0.2),
 libghc-monoid-extras-dev (<< 0.6),
 libghc-monoid-extras-prof,
 libghc-newtype-generics-dev (>= 0.5.3),
 libghc-newtype-generics-dev (<< 0.6),
 libghc-newtype-generics-prof,
 libghc-semigroups-dev (>= 0.8),
 libghc-semigroups-dev (<< 0.20),
 libghc-semigroups-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-monoid-extras-doc,
 libghc-newtype-generics-doc,
 libghc-semigroups-doc,
Standards-Version: 4.5.0
Homepage: https://hackage.haskell.org/package/dual-tree
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-dual-tree
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-dual-tree]

Package: libghc-dual-tree-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Rose trees with cached and accumulating monoidal annotations${haskell:ShortBlurb}
 Rose (n-ary) trees with both upwards- (i.e. cached) and downwards-traveling
 (i.e. accumulating) monoidal annotations.
 .
 Abstractly, a DUALTree is a rose (n-ary) tree with data at leaves, data at
 internal nodes, and two types of monoidal annotations, one travelling "up" the
 tree and one traveling "down".
 .
 ${haskell:Blurb}

Package: libghc-dual-tree-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Rose trees with cached and accumulating monoidal annotations${haskell:ShortBlurb}
 Rose (n-ary) trees with both upwards- (i.e. cached) and downwards-traveling
 (i.e. accumulating) monoidal annotations.
 .
 Abstractly, a DUALTree is a rose (n-ary) tree with data at leaves, data at
 internal nodes, and two types of monoidal annotations, one travelling "up" the
 tree and one traveling "down".
 .
 ${haskell:Blurb}

Package: libghc-dual-tree-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Rose trees with cached and accumulating monoidal annotations${haskell:ShortBlurb}
 Rose (n-ary) trees with both upwards- (i.e. cached) and downwards-traveling
 (i.e. accumulating) monoidal annotations.
 .
 Abstractly, a DUALTree is a rose (n-ary) tree with data at leaves, data at
 internal nodes, and two types of monoidal annotations, one travelling "up" the
 tree and one traveling "down".
 .
 ${haskell:Blurb}
